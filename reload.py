"""
reload.py: serve documents from a directory and reload open pages
when files are modified

Supports Python 3.7+

Install dendencies with: pip install aiohttp watchgod
"""

import asyncio
from collections import defaultdict
from concurrent.futures import ThreadPoolExecutor
from json import dumps, loads
from mimetypes import guess_type
from pathlib import Path
import sys

from aiohttp import web, WSCloseCode, WSMsgType
from watchgod import awatch


def static_file_factory(directory, index="index.html"):
    """ Create a handler for serving static files from a directory """

    async def static_file_handler(request):
        """
        Serve static files from a directory

        If an HTML document is requested, return an Response containing
        the file contents with embedded reload script. Otherwise, return
        a FileResponse.
        """
        path = directory.joinpath(request.path[1:])

        if path.is_dir():
            path = path.joinpath(index)

        if path.is_file():
            mime = guess_type(path)[0]

            if mime == "text/html":
                updated_text = path.read_text().replace("</head>", "<script src=\"/reload.js\"></script></head>")
                return web.Response(text=updated_text, content_type="text/html")
            else:
                return web.FileResponse(path)
        else:
            raise web.HTTPNotFound()

    return static_file_handler


async def websocket_handler(request):
    """
    Websocket handler which takes care of registering documents and
    their respective stylesheets
    """
    ws = web.WebSocketResponse()

    await ws.prepare(request)

    async def websocket_logic(payload):
        path = payload["path"][1:]
        rel_path = path + "index.html" if len(path) == 0 or path[-1] else path

        if payload["type"] == "open":
            for style_file in payload["stylesheets"]:
                request.app["stylesheets"][style_file].add(ws)

            request.app["documents"][rel_path].add(ws)
        elif payload["type"] == "close":
            for style_file in payload["stylesheets"]:
                request.app["stylesheets"][style_file].remove(ws)

            request.app["documents"][rel_path].remove(ws)

            await ws.close()
        else:
            await ws.send_str(msg.data)

    async for msg in ws:
        if msg.type == WSMsgType.TEXT:
            await websocket_logic(loads(msg.data))
        elif msg.type == WSMsgType.ERROR:
            print("ws connection closed with exception %s" % ws.exception())

    return ws


async def websocket_shutdown(app):
    """ Clean up all open websockets on server shutdown """
    for handler_set in app["documents"].values():
        for ws in handler_set:
            await ws.close(code=WSCloseCode.GOING_AWAY,
                        message="Server shutdown")


async def web_main(app, host, port):
    """
    A short async function which essentially copies what
    aiohttp.web.run_app does internally
    """
    runner = web.AppRunner(app)

    await runner.setup()

    site = web.TCPSite(runner, host, port)

    await site.start()

    # Keep the web server alive how  it
    try:
        while True:
            await asyncio.sleep(3600)
    finally:
        await runner.cleanup()


async def watcher_main(app, path, callbacks):
    """
    Handle file modifications by sending websocket messages to
    relevant files
    """
    loop = asyncio.get_running_loop()

    with ThreadPoolExecutor(max_workers=2) as executor:
        async for changes in awatch(path):
            for change_type, change_file in changes:
                # Run callbacks if any associated with path
                if change_file in callbacks:
                    await loop.run_in_executor(executor, callbacks[change_file])

                change_path = Path(change_file)

                rel_path = str(change_path.relative_to(path))

                # If document modified, reload page
                if rel_path in app["documents"]:
                    for handler in app["documents"][rel_path]:
                        await handler.send_str(dumps({
                            "type": "reload",
                            "path": rel_path}))
                # If stylesheet modified, send new css to pages
                elif rel_path in app["stylesheets"]:
                    for handler in app["stylesheets"][rel_path]:
                        await handler.send_str(dumps({
                            "type": "style_change",
                            "path": rel_path,
                            "text": change_path.read_text()}))
                # Otherwise, reload all pages
                else:
                    for handler_set in app["documents"].values():
                        for handler in handler_set:
                            await handler.send_str(dumps({
                                "type": "reload",
                                "path": list(changes)}))


if __name__ == "__main__":
    source_path = Path(sys.argv[1])

    if not source_path.is_dir():
        sys.exit(f"Path '{source_path}' is not a directory")

    host, port = "localhost", 8080

    print(f"Serving at http://{host}:{port}")

    app = web.Application()

    app["documents"] = defaultdict(set)
    app["stylesheets"] = defaultdict(set)

    async def serve_reload_script(request):
        return web.FileResponse("reload.js")

    app.add_routes([
        web.get("/ws", websocket_handler),
        web.get("/reload.js", serve_reload_script),
        web.get("/{tail:.*}", static_file_factory(source_path))
    ])

    app.on_shutdown.append(websocket_shutdown)

    callbacks = {}

    async def main():
        await asyncio.gather(
            watcher_main(app, source_path, callbacks),
            web_main(app, host, port))

    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        print("\rGoodbye!")
