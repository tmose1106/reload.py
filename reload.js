(function() {
    // Start scope
    const ws = new WebSocket(`ws://${window.location.host}/ws`);

    const stylesheets = Array.from(document.head.getElementsByTagName('link'))
        .filter((element) => element.rel === 'stylesheet')
        .map((element) => new URL(element.href).pathname.slice(1));

    const styleMap = new Map(stylesheets.map(
        (path) => [path, document.createElement('style')]));

    Array.from(styleMap.values()).forEach(
        (element) => document.head.appendChild(element));

    ws.addEventListener('open', () => {
        ws.send(
            JSON.stringify({
                'type': 'open',
                'path': window.location.pathname,
                'stylesheets': stylesheets
        }));
    });

    ws.addEventListener('message', (event) => {
        const msg = JSON.parse(event.data);

        if (msg.type === 'reload') {
            window.location.reload();
        } else if (msg.type === 'style_change') {
            const styleElement = styleMap.get(msg.path),
                styleText = document.createTextNode(msg.text);

            if (styleElement.childNodes.length > 0) {
                styleElement.replaceChild(styleText, styleElement.firstChild);
            } else {
                styleElement.appendChild(document.createTextNode(msg.text));
            }
        } else {
            console.log('Unknown message type', event.data);
        }
    });

    window.addEventListener('beforeunload', () => {
        ws.send(
            JSON.stringify({
                'type': 'close',
                'path': window.location.pathname,
                'stylesheets': stylesheets
        }));
    });

    // End scope
})();