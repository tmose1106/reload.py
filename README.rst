*********
reload.py
*********

Asynchronous Live Reloading Web Development Server

Installation
============

The server supports Python 3.7+

Install dependencies using pip:

::

  pip install aiohttp watchgod

Usage
=====

::

  python3 reload.py [PATH]

Give it a try by serving the `src` directory and edit some files to see what
happens.

::

  python3 reload.py src